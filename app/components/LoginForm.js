import React from 'react'
import {
    View,
    StyleSheet,
    Text,
    TextInput,
    StatusBar,
    TouchableOpacity,
} from 'react-native'

import {
    Actions
} from 'react-native-router-flux'

class LoginForm extends React.Component {
    render(){
        return (
            <View style={styles.container}>
                <StatusBar
                    barStyle="light-content"
                />
                <TextInput 
                    placeholder="username or email"
                    placeholderTextColor="rgba(255,255,255,0.7)"
                    style={styles.input} 
                    returnKeyType="next"
                    keyboardType="email-address"
                    autoCapitalize="none"
                    autoCorrect={false}
                    onSubmitEditing={() => this.passwordInput.focus()}
                    underlineColorAndroid="rgba(0,0,0,0)"
                    onChangeText={(username) => this.username = username}
                    />
                <TextInput
                    placeholder="password" 
                    placeholderTextColor="rgba(255,255,255,0.7)"
                    style={styles.input}
                    secureTextEntry={true}
                    returnKeyType="go"
                    ref={(input) => this.passwordInput = input}
                    underlineColorAndroid="rgba(0,0,0,0)" 
                    onChangeText={(password) => this.password = password}
                    />

                <TouchableOpacity style={styles.buttonContainer} onPress={
                    () => this.props.handleLogin(
                    { 
                        "username": this.username,
                        "password" :  this.password,
                    })
                }>
                    <Text style={styles.buttonText}>Login</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 20,
    },
    input: {
        fontSize: 18,
        height: 42,
        marginBottom: 15,
        color: "#fff",
        backgroundColor: "rgba(255,255,255,0.2)",
        paddingHorizontal: 10,
    },
    buttonContainer: {
        backgroundColor: "#2980b9",
        paddingVertical: 15,
    },
    buttonText: {
        fontSize: 20,
        textAlign: "center",
        color: "#fff",
        fontWeight: "700",   
    }
})

export default LoginForm