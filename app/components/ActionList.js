import React from 'react'
import {
    FlatList,
} from 'react-native'
import ActionButton from './ActionButton'
import ActionButtonEmoji from './ActionButtonEmoji'

class ActionList extends React.Component {
    constructor(props){
        super(props)
    }

    render(){
        return (
            <FlatList
                data={this.props.actions}
                renderItem={({item}) => ((this.props.rating) ? <ActionButtonEmoji handleActions={this.props.handleActions} item={item} /> : <ActionButton handleActions={this.props.handleActions} item={item} /> )}
                keyExtractor={(item, idx) => item.id }
                horizontal={(this.props.actions.length > 4) ? true : false}
            />
        )
    }
}

export default ActionList