import React from 'react'
import {
    View,
    Text,
    StyleSheet,
    Alert,
    TouchableOpacity
} from 'react-native'

class ActionButtonEmoji extends React.Component {
    constructor(props){
        super(props);
    }
    
    render(){
        return (
            <TouchableOpacity 
            key={this.props.item.id}
            onPress={() => this.props.handleActions(this.props.item.label, this.props.item.action)}
            >
                <View
                style={styles.actionButtonEmoji}                
                >
                    <Text
                    style={styles.actionEmoji}
                    > {this.props.item.label}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    actionEmoji:{
        fontSize: 36,
        textAlign: "center",
    },
    actionButtonEmoji: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: "#f5f5f5",
        padding: 8,
        borderColor: "#333",
        borderRadius: 100,
        margin: 5,
    },
})

export default ActionButtonEmoji