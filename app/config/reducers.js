import {combineReducers} from 'redux'
import userReducer from './reducers/userReducer'
import chatReducer from './reducers/chatReducer'
// import messagesReducer from './reducers/messagesReducer'

const reducers = combineReducers({
    user: userReducer,
    chat: chatReducer,
})

export default reducers