import {createStore} from 'redux'
import reducer from './reducers'
import middleware from './middleware'

let store = createStore(reducer, middleware)

export default store