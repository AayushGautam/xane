import { storeToken} from '../storage'

const initialState = {
    isAuthenticating: false,
    isAuthenticated: false,
    token: null,
    info: {
        __id: 2,
        name: 'Sansa Stark',
        avatar: 'https://www.venmond.com/demo/vendroid/img/avatar/big.jpg',
    },
}

const userReducer = (state=initialState, action) => {
    switch(action.type){
        case 'AUTHENTICATE_USER_PENDING': {
            state = {...state, isAuthenticating: true}
            break;
        }
        case 'AUTHENTICATE_USER_FULFILLED' : {
            if(action.payload){
                storeToken(action.payload.data.token)
                state = {...state,isAuthenticating: false ,isAuthenticated: true, token: action.payload.data.token}
            }
            else{
                state = {...state, isAuthenticating: false, isAuthenticated: false}
            }
            break;
        }
        case 'AUTHENTICATE_USER_REJECTED': {
            state = {...state, isAuthenticating: false, isAuthenticated: false}
            break;
        }
    }
    return state
}

export default userReducer