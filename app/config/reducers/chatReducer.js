import { GiftedChat, GiftedAvatar } from 'react-native-gifted-chat';

const initialState = {
    messages : [],
    actions : [{
        id: 1,
        label: "Get Started",
        action: "GET_STARTED"
    }],
    isRating: false,
}

const chatReducer = (state=initialState, action) => {
    switch (action.type) {
        case 'APPEND_MESSAGE':
            newState = {...state}
            newState.messages = GiftedChat.append(state.messages,action.payload.message) 
            return newState;
        
        case 'SEND_MESSAGE_ON_SERVER_FULFILLED':
            newState = {...state, isRating: action.payload.data.message.rating }
            newState.messages = GiftedChat.append(state.messages,action.payload.data.message.message) 
            newState.actions = action.payload.data.message.actions
            return newState;
        default:
            return state
    }
}

export default chatReducer