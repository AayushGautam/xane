import axios from 'axios'

export function authenticateUser(payload) {
    console.log(payload)
    return {
        type: 'AUTHENTICATE_USER',
        payload: axios({
            url: 'https://dashboard.munna.io/api/obtain-auth-token/',
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            data: {
                username: (payload.username != undefined) ? payload.username : "",
                password: (payload.password != undefined) ? payload.password : "",
            }
        }).catch((error) => {
            alert(error.message)
        })
    }
}