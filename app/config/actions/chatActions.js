import axios from "axios";
import {getToken} from '../storage'

export function sendMessage(message){
    return function(dispatch){
       dispatch({
           type: "APPEND_MESSAGE",
           payload: {
               message: message,
           }
       })

       dispatch({
           type: "SEND_MESSAGE_ON_SERVER",
           payload: axios({
               url: 'https://dashboard.munna.io/botlogic/app/d40f06e11f6d4fc185703a4e1ced1ce2b6ba525b772a306c5b/',
               method: 'POST',
               headers: {
                   'Autharization': 'Token ' + getToken(),
                   'Accept': 'application/json',
                   'Content-Type': 'application/json',
               },
               data: {
                   message: message,
               },
           }).catch((error) => {
               alert(error.message)
           })
       })
    }
}