import { AsyncStorage } from "react-native"
import {ACCESS_TOKEN} from './constants'

export async function storeToken(accessToken){
    try {
        await AsyncStorage.setItem(ACCESS_TOKEN, accessToken)
    } catch (error) {
        console.log("user auth data nol stored")
    }
}

export async function getToken() {
    try {
        let token = await AsyncStorage.getItem(ACCESS_TOKEN, accessToken)
        return token
    } catch (error) {
        console.log("something went wrong: line 16 storage.js")
    }
    return null
}

export async function removeToken() {
    try {
        await AsyncStorage.removeItem(ACCESS_TOKEN)
    } catch (error) {
        console.log("something went wrong: line 24 storage.js")
    }
}
