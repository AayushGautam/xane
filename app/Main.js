/**
 * Munna Chat App for ios and android
 * https://munna.io/app
 */

import React, { Component } from 'react'
import { Provider } from 'react-redux'
import App from "./App"
import store from "./config/store"

class Main extends Component {
    render() {
        return (
            <Provider store={store}>
                <App />
            </Provider>
        )
    }
}

export default Main