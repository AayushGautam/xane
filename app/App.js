import React, { Component } from 'react'

import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';

import {
    Router,
    Stack,
    Scene,
} from 'react-native-router-flux'

import { connect } from 'react-redux'

import Home from './screens/Home'
import Login from './screens/Login'
import { authenticateUser } from "./config/actions/userActions"

@connect((store) => {
	return {
		isAuthenticated: store.user.isAuthenticated,
		isAuthenticating: store.user.isAuthenticating,
	}
})
class App extends Component {
	constructor(props){
		super(props)
		this.handleLogin = this.handleLogin.bind(this)
	}

	handleLogin(payload){
		this.props.dispatch(authenticateUser(payload))
	}

	render() {
		if(this.props.isAuthenticated){
			return (
				<Router>
					<Stack key="root">
						<Scene key="home" component={Home} title="Chat with Munna"/>
					</Stack>
				</Router>
			);
		}
		else{
			return <Login isloading={this.props.isAuthenticating} handleLogin={this.handleLogin}/>
		}
	}
}

export default App

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#fafafa',
  },
});
