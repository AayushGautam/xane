import React from 'react'
import {
    View,
    Text,
    Alert,
    Image,
    StyleSheet,
    KeyboardAvoidingView,
} from 'react-native'
import {Spinner} from 'native-base'

import LoginForm from '../components/LoginForm'

class Login extends React.Component {
    constructor(props) {
        super(props);
    }
    
    render(){
        let pic = {
            uri: 'https://munna-storage.s3.amazonaws.com/static/logo/munna-200.png'
        };

        return (
            <KeyboardAvoidingView style={styles.container}>
                <View style={styles.logoContainer}>
                    <Image 
                        style={styles.logo}
                        source={pic}
                    />
                    <Text style={styles.title}>A chat client for Munna</Text>
                </View>
                <View style={styles.formContainer}>
                    { (!this.props.isloading) ? <LoginForm handleLogin={this.props.handleLogin} /> : null}
                </View>
                <View>
                    { (this.props.isloading) ?  <Spinner /> : null }
                </View>
            </KeyboardAvoidingView>
        )
    }
}

export default Login

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#3498db"
    },
    logoContainer:{
        flexGrow: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    logo: {
        width: 150,
        height: 150,
        borderRadius: 75,
    },
    title: {
        color: "#fff",
        fontSize: 26,
        marginTop: 10,
        textAlign: "center",
        opacity: 0.8,
    }

})