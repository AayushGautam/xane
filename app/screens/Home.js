import React from 'react'
import {
  Alert,
  View,
  StyleSheet,
} from 'react-native'

import {connect} from 'react-redux'
import { GiftedChat } from 'react-native-gifted-chat';
import uuid from 'uuid'
import ActionList from '../components/ActionList'
import * as chatActions from '../config/actions/chatActions'

//  DELETE BELOW THIS
const actions = [
    {
        id: 1,
        label: "Ask for a feedback",
        action: "ASK_FEEDBACK"
    },
    {
        id: 2,
        label: "Start an Appraisals Cycle",
        action: "360_FEEDBACK"
    },
    {
        id: 3,
        label: "Start an Organisation Survey",
        action: "ORG_SURVEY"
    }
]

const ratings = [
    {
        id: 1,
        label: "😍",
        action: "1"
    },
    {
        id: 2,
        label: "😀",
        action: "2"
    },
    {
        id: 3,
        label: "😐",
        action: "3"
    },
    {
        id: 4,
        label: "😰",
        action: "4"
    },
    {
        id: 5,
        label: "😢",
        action: "5"
    }
]

function get_reply(message){
    let ret;
    switch(message){
        case 'ASK_FEEDBACK':
            ret = {
                message: {
                    _id: Math.random(),
                    text: 'Hello Sansa',
                    createdAt: new Date(),
                    user: {
                        _id: 2,
                        name: 'Munna',
                        avatar: 'https://munna-storage.s3.amazonaws.com/static/logo/munna-200.png',
                    }
                },
                actions: [],
                rating: false,
            }    
            break;
        case '360_FEEDBACK':
            ret = {
                message: {
                    _id: Math.random(),
                    text: 'Hello Sansa',
                    createdAt: new Date(),
                    user: {
                        _id: 2,
                        name: 'Munna',
                        avatar: 'https://munna-storage.s3.amazonaws.com/static/logo/munna-200.png',
                    }
                },
                actions: [],
                rating: false,
            }        
            break;
        case 'ORG_SURVEY':
            ret = {
                message: {
                    _id: Math.random(),
                    text: 'Organisation Survey has been Started by the admin',
                    createdAt: new Date(),
                    user: {
                        _id: 2,
                        name: 'Munna',
                        avatar: 'https://munna-storage.s3.amazonaws.com/static/logo/munna-200.png',
                    }
                },
                actions: [
                    {
                        id: 1,
                        label: "Okay",
                        action: "ORG_OKAY_1"
                    },
                ],
                rating: false,
            }
            break;
        case 'ORG_OKAY_1':
            ret = {
                message: {
                    _id: Math.random(),
                    text: 'How likely are you to recommend working at Profformance?',
                    createdAt: new Date(),
                    user: {
                        _id: 2,
                        name: 'Munna',
                        avatar: 'https://munna-storage.s3.amazonaws.com/static/logo/munna-200.png',
                    }
                },
                actions: ratings,
                rating: true,
            }
            break;
        case 'HMMM' :
            ret = {
                message: {
                    _id: Math.random(),
                    text: 'How do you feel about the oppurtunities you\'ve had learn and grow in a past month',
                    createdAt: new Date(),
                    user: {
                        _id: 2,
                        name: 'Munna',
                        avatar: 'https://munna-storage.s3.amazonaws.com/static/logo/munna-200.png',
                    }
                },
                actions: ratings,
                rating: true,
            }
            break;
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
            ret = {
                message: {
                    _id: Math.random(),
                    text: 'Okay! please elaborate more on your rating.',
                    createdAt: new Date(),
                    user: {
                        _id: 2,
                        name: 'Munna',
                        avatar: 'https://munna-storage.s3.amazonaws.com/static/logo/munna-200.png',
                    }
                },
                actions: [
                ],
                rating: true,
            }
            break;
        case 'GET_STARTED':
            ret = {
                message: {
                    _id: Math.random(),
                    text: 'Hi!, What can I do for you?',
                    createdAt: new Date(),
                    user: {
                        _id: 2,
                        name: 'Munna',
                        avatar: 'https://munna-storage.s3.amazonaws.com/static/logo/munna-200.png',
                    }
                },
                actions : [
                    {
                        id: 1,
                        label: "Ask for a feedback",
                        action: "ASK_FEEDBACK"
                    },
                    {
                        id: 2,
                        label: "Start an Appraisals Cycle",
                        action: "360_FEEDBACK"
                    },
                    {
                        id: 3,
                        label: "Start an Organisation Survey",
                        action: "ORG_SURVEY"
                    }
                ],
                rating: false,
            }
            break;
        case 'TEXT' :
            ret = {
                message: {
                    _id: Math.random(),
                    text: 'Thanks for your response',
                    createdAt: new Date(),
                    user: {
                        _id: 2,
                        name: 'Munna',
                        avatar: 'https://munna-storage.s3.amazonaws.com/static/logo/munna-200.png',
                    }
                },
                actions : [
                    {
                        id: 1,
                        label: "hmmm",
                        action: "HMMM"
                    },
                ],
                rating: false,
            }
            break;
        case 'TEXT_1' :
            ret = {
                message: {
                    _id: Math.random(),
                    text: 'Thanks for your response \n Organisation Survey is complete now!',
                    createdAt: new Date(),
                    user: {
                        _id: 2,
                        name: 'Munna',
                        avatar: 'https://munna-storage.s3.amazonaws.com/static/logo/munna-200.png',
                    }
                },
                actions : [
                    {
                        id: 1,
                        label: "Start Again",
                        action: "GET_STARTED"
                    },
                ],
                rating: false,
            }
            break;
        default:
            ret = {
                message: {
                    _id: Math.random(),
                    text: 'Sorry I didn\'t get that',
                    createdAt: new Date(),
                    user: {
                        _id: 2,
                        name: 'Munna',
                        avatar: 'https://munna-storage.s3.amazonaws.com/static/logo/munna-200.png',
                    }
                },
                actions : [
                    {
                        id: 1,
                        label: "Try Again",
                        action: "GET_STARTED"
                    },
                ],
                rating: false,
            }   
    }
    return ret;
}
//  DELETE ABOVE THIS

@connect((store) => {
    console.log(store)
    return {
        info : store.user.info,
        messages: store.chat.messages,
        actions: store.chat.actions,
        isRating: store.chat.isRating,
    }
})
class Home extends React.Component {
    constructor(props){
        super(props)
        this.handleActions = this.handleActions.bind(this)
        this.count = 0;
    }

    onSend(messages = [], action) {
        this.props.dispatch(chatActions.sendMessage(messages))    
    }

    handleActions(label, action){
        let reply = {
            user: this.props.info,
            createdAt: new Date(),
            _id: uuid.v4(),
            text: label,
        }
        this.onSend([reply], action)
    }

    render() {
        return (
            <GiftedChat
                messages={this.props.messages}
                onSend={(messages) => this.onSend(messages)}
                user={this.props.info}
                showUserAvatar={false}
                loadEarlier={false}
                onLoadEarlier={() => Alert.alert("Hello")}
                isLoadingEarlier={false}
                renderFooter={() => <ActionList actions={this.props.actions} rating={this.props.isRating} handleActions={this.handleActions}/>}
                />
        );
    }

}

export default Home;